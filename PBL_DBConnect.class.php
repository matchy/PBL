<?php
/*
 * Copyright (c) 2013 MACHIDA 'matchy' Hideki
 * Copyright (c) 2011,2012 HAKKO Development Co.,Ltd. az'Ciel division.
 * All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * データベースアクセスクラス定義.
 *
 * @package pbl
 */

require_once(ADODB_DIR . 'adodb-errorpear.inc.php');
require_once(ADODB_DIR . 'adodb.inc.php');

/**
 * データベース接続クラス.
 *
 * @package pbl
 */
class PBL_DBConnect {

  static $instance__;

  /**
   * データベースコネクションハンドル
   *
   * @access private
   * @type ADOConnection
   */
  var $conn_ = null;

  /**
   * 文字コードの指定
   *
   * @access private
   */
  function PBL_setNames() {
    // この部分は MySQL 依存になっているので注意
    if (function_exists('mysql_set_charset')) {
      mysql_set_charset('UTF8', $this->conn_->_connectionID);
    } else {
      $this->conn_->Execute('SET NAMES UTF8');
    }
  }

  /**
   * コンストラクタ
   *
   * データベースアクセスクラスを構築し、
   * データベースとの接続を確立する。
   * このクラスは Singleton クラスなので、
   * 直接コンストラクタを呼ばないこと。
   *
   * @access private
   */
  function PBL_DBConnect() {
    $this->conn_ =& ADONewConnection(PBL_DSN);
    $this->PBL_setNames();
    $this->conn_->Execute('SET TRANSACTION ISOLATION LEVEL READ COMMITTED');
  }

  /**
   * データベースとの接続を閉じる
   *
   * @access public
   */
  function close() {
    if ($this->conn_) {
      $this->conn_->Close();
      $this->conn_ = null;
    }
  }

  /**
   * DB INSERT / UPDATE 用文字の quote を解除する
   *
   * @access private
   * @param string 入力文字列
   * @return string 変換済み文字列
   */
  function unQuote($input) {
    $regexp = "/(^')|('$)/";
    return preg_replace($regexp, '', $input);
  }

  /**
   * インスタンスの取得 (singleton)
   *
   * 本クラスの singleton インスタンスを取得する.
   * コンストラクタを明示的に呼び出さず、本メソッドを使用して
   * インスタンスを取得すること
   *
   * @access public
   * @static
   * @return PBL_Databse のインスタンス
   */
  function &getInstance() {
    if (!isset($instance__)) {
      $instance__ =& new PBL_DBConnect();
    }
    return $instance__;
  }

  /**
   * データベースコネクションハンドルの取得
   *
   * 本クラスが管理しているデータベースコネクションハンドル
   * を返却する。
   * 通常は getInstance ではなく、こちらを使用する。
   *
   * @access public
   * @static
   * @return ADOConnection コネクションハンドル
   */
  function &getConnection() {
    $instance =& PBL_DBConnect::getInstance();
    return $instance->conn_;
  }

}

/*
 * -*- settings for emacs. -*-
 * Local Variables:
 *   mode:php
 *   indent-tabs-mode: nil
 *   c-basic-offset: 2
 * End:
 */
?>
