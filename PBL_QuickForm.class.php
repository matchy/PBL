<?php
/*
 * Copyright (c) 2004-2005,2015 MACHIDA 'matchy' Hideki
 * Copyright (c) 2006 Japan Computer Co.,Ltd.
 * Copyright (c) 2008 az'Ciel HAKKO Co,Ltd.
 * All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * HTML_QuickForm ラッピングクラス.
 *
 * PEAR::HTML_QuickForm を日本語環境で使うための
 * 派生クラスなど
 *
 * @package pbl
 */

require_once('HTML/QuickForm.php');
require_once('HTML/QuickForm/textarea.php');
require_once('HTML/QuickForm/checkbox.php');
require_once('HTML/QuickForm/radio.php');
require_once('HTML/QuickForm/radio.php');
require_once('HTML/QuickForm/Rule/Range.php');
require_once('HTML/QuickForm/RuleRegistry.php');

/**
 * QuickForm クラス.
 *
 * PEAR::HTML_QuickForm のメッセージ部分を日本語にしたもの。
 *
 * @package pbl
 * @access public
 */
class PBL_QuickForm extends HTML_QuickForm {

  /**
   * メッセージを日本語に設定した HTML_QuickForm 派生クラスを
   * 構築する。
   *
   * @access public
   * @see HTML_QuickForm::HTML_QuickForm()
   */
  function PBL_QuickForm($formName='', $method='',
                         $action='',
                         $target='_self', $attributes=null,
                         $trackSubmit = false) {

    parent::HTML_QuickForm($formName, $method,
                           $action, $target, $attributes,
                           $trackSubmit);

    $this->setJsWarnings("入力された情報が正しくありません。",
                         "入力内容をご確認下さい。");
		
    $requirednote  = '<span class="warning">*</span> ';
    $requirednote .= '<span class="warningnote">が付いている項目は必ずご入力ください。</span>';
    $this->setRequiredNote($requirednote);
  }

}

/**
 * HTML_QuickForm_textarea の改行処理修正版
 *
 * 標準の HTML_QuickForm_textarea クラスは改行があった場合の処理が
 * 正しく行われないようなので、一部を修正したものである。
 * 標準の HTML_QuickForm_textarea と置き換わって動作するため、特に
 * 意識すること無く本クラスが使用される
 *
 * @package pbl
 * @access public
 */
class PBL_QuickForm_textarea extends HTML_QuickForm_textarea {

  function PBL_QuickForm_textarea($elementName=null,
				  $elementLabel=null,
				  $attributes=null) {
    parent::HTML_QuickForm_textarea($elementName, $elementLabel, $attributes);
  }

  /**
   * HTML レンダリングメソッド
   *
   * @access public
   * @see HTML_QuickForm_textarea::toHtml()
   */
  function toHtml() {
    if ($this->_flagFrozen) {
      return $this->getFrozenHtml();
    } else {
      return $this->_getTabs() .
        '<textarea' . $this->_getAttrString($this->_attributes) . '>' .
        preg_replace("/(\r\n|\n|\r)/", "\n",
                     htmlspecialchars($this->_value)) .
        '</textarea>';
    }
  }

}

/**
 * HTML_QuickForm_checkbox の value 属性対応修正版
 *
 * 標準の HTML_QuickForm_checkbox クラスは value 属性に値を設定できない
 * ようなので、一部を修正したものである。
 *
 * 標準の HTML_QuickForm_checkbox と置き換わって動作するため、特に
 * 意識すること無く本クラスが使用される。
 *
 * @package pbl
 * @access public
 */
class PBL_QuickForm_checkbox extends HTML_QuickForm_checkbox {

  /**
   * QuickForm チェックボックスの構築
   *
   * @access public
   * @see HTML_QuickForm_checkbox::HTML_QuickForm_checkbox()
   */
  function PBL_QuickForm_checkbox($elementName=null,
                                  $elementLabel=null,
                                  $text='',
                                  $attributes=null) {

    parent::HTML_QuickForm_checkbox($elementName,
                                    $elementLabel, $text, $attributes);

    if (isset($attributes['value'])) {
      $this->updateAttributes(array('value'=>$attributes['value']));
    }
  }

}

// 派生クラスで機能を置き換える
$GLOBALS['HTML_QUICKFORM_ELEMENT_TYPES']['textarea'] =
  array(__FILE__, 'PBL_QuickForm_textarea');
$GLOBALS['HTML_QUICKFORM_ELEMENT_TYPES']['checkbox'] =
  array(__FILE__, 'PBL_QuickForm_checkbox');


/**
 * 文字数による文字長チェックツール
 *
 * @package pbl
 */
class PBL_QuickForm_Rule_Range extends HTML_QuickForm_Rule_Range {
  /**
   * 文字数チェックバリデーション
   *
   * @param     string    $value      チェックする値
   * @param     mixed     $options    Int for length, array for range
   * @access    public
   * @return    boolean   true if value is valid
   */
  function validate($value, $options) {
    $length = mb_strlen($value);
    switch ($this->name) {
    case 'minlength':
      return ($length >= $options);
    case 'maxlength':
      return ($length <= $options);
    default:
      return ($length >= $options[0] && $length <= $options[1]);
    }
  }
}

// 上書き登録
$range =& new PBL_QuickForm_Rule_Range();
$reg =& HTML_QuickForm_RuleRegistry::singleton();
$reg->registerRule('range', NULL, $range);
$reg->registerRule('minlength', NULL, $range);
$reg->registerRule('maxlength', NULL, $range);

/*
 * -*- settings for emacs. -*-
 * Local Variables:
 *   mode:php
 *   indent-tabs-mode: nil
 *   c-basic-offset: 2
 * End:
 */
?>
