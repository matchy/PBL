<?php
/*
 * Copyright (c) 2004-2005,2013 MACHIDA 'matchy' Hideki
 * Copyright (c) 2006 Japan Computer Co.,Ltd.
 * Copyright (c) 2008 az'Ciel HAKKO Co.,Ltd.
 * All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * メール送信クラス.
 *
 * 日本語を含むメールを送信する。
 * 要 mbstring 拡張
 *
 * @package pbl
 */

/**
 * メール送信クラス
 *
 * @package pbl
 * @access public
 */
class PBL_Mailer {

  /**
   * From ヘッダ
   *
   * @var string
   * @access private
   */
  var $from_ = "";
  
  /**
   * To ヘッダ
   *
   * @var string
   * @access private
   */
  var $to_ = "";
  
  /**
   * Cc ヘッダ
   *
   * @var string
   * @access private
   */
  var $cc_ = "";

  /**
   * Bcc ヘッダ
   *
   * @var string
   * @access private
   */
  var $bcc_ = "";

  /**
   * Subject ヘッダ
   *
   * @var string
   * @access private
   */
  var $subject_ = "";

  /**
   * Envelope-From
   *
   * @var string
   * @access private
   */
  var $sender_ = "";

  /**
   * 本文
   *
   * @var string
   * @access private
   */
  var $body_ = "";

  /**
   * UserAgent
   *
   * @var string
   * @access private
   */
  var $user_agent_ = "";


  /**
   * From ヘッダ等用に文字列を生成する
   *
   * @access private
   * @param string $addr メールアドレス等
   * @param string $name 名前など、MIME エンコーディングする必要のある文字列
   */
  function makeHeader($addr, $name=null) {
    $addr = trim($addr);

    if (!empty($name)) {
      $result = mb_convert_kana(trim($name));
      $result = mb_encode_mimeheader($result);

      $result = "{$result} <{$addr}>";

    } else {
      $result = $addr;
    }
    return $result;
  }

  /**
   * 本文の変換
   *
   * @access public
   * @param string $src 本文文字列
   * @return string 変換後の本文文字列
   */
  function convertBody($src) {
    $result = $src;
    $lang = mb_language();
    if (preg_match("/^japan/i", $lang) || preg_match("/^ja/i", $lang)) {
      $result = mb_convert_encoding($result,
                                    'ISO-2022-JP',
                                    mb_internal_encoding());
    }
    return $result;
  }

  /**
   * コンストラクタ
   *
   * @access public
   * @param string $subject Subject 文字列
   * @param string $body    本文
   */
  function PBL_Mailer($subject, $body) {
    $this->subject_ = mb_encode_mimeheader(mb_convert_kana(trim($subject)));
    $this->body_    = $this->convertBody(mb_convert_kana(trim($body)));
  }

  /**
   * 送りもと (from) の設定
   *
   * @access public
   * @param string $addr 宛先アドレス
   * @param string $name 宛先名
   */
  function setFrom($addr, $name=null) {

    $from = $this->makeHeader($addr, $name);
    $this->from_ = $from;
  }

  /**
   * Envelope-From の設定
   *
   * @access public
   * @param string $addr 宛先アドレス
   */
  function setSender($addr) {
    $this->sender_ = $addr;
  }

  /**
   * 宛先の追加
   *
   * @access public
   * @param string $addr 宛先アドレス
   * @param string $name 宛先名
   */
  function addTo($addr, $name=null) {

    $to = $this->makeHeader($addr, $name);

    if (empty($this->to_)) {
      $this->to_ = $to;
    } else {
      $this->to_ .= ", " . $to;
    }
  }

  /**
   * Cc の追加
   *
   * @access public
   * @param string $addr 宛先アドレス
   * @param string $name 宛先名
   */
  function addCc($addr, $name=null) {

    $cc = $this->makeHeader($addr, $name);

    if (empty($this->cc_)) {
      $this->cc_ = $cc;
    } else {
      $this->cc_ .= ", " . $cc;
    }
  }

  /**
   * Bcc の追加
   *
   * @access public
   * @param string $addr 宛先アドレス
   * @param string $name 宛先名
   */
  function addBcc($addr, $name=null) {

    $bcc = $this->makeHeader($addr, $name);

    if (empty($this->bcc_)) {
      $this->bcc_ = $bcc;
    } else {
      $this->bcc_ .= ", " . $bcc;
    }
  }

  /**
   * Content-Type の取得
   *
   * @access public
   * @return string Content-Type 文字列
   */
  function getContentType() {
    $lang = mb_language();
    if (preg_match("/^japan/i", $lang) || preg_match("/^ja/i", $lang)) {
      return "Content-Type: text/plain; charset=ISO-2022-JP";
    }
    return "Content-Type: text/plain; charset=us-ascii";
  }

  /**
   * 送信実行
   *
   * @access public
   * @return boolean true:成功 / false:失敗
   */
  function send() {
    $to   = $this->to_;
    $bcc  = $this->bcc_;
    $cc   = $this->cc_;
    $from = $this->from_;
    if ((empty($to) && empty($bcc) && empty($cc)) || empty($from)) {
      return false;
    }

    $user_agent = $this->user_agent_;

    $body    = $this->body_;
    $subject = $this->subject_;
    $sender  = $this->sender_;

    if (empty($sender)) {
      if (preg_match("/([0-9a-z\-\._\/\?]+@[0-9a-z\-\._\/\?]+)/i",
                     $from, $matches)) {

        $sender = $matches[1];
      } else {
        return false;
      }
    }
 
    $header  = "Error-To: ${sender}\n";
    $header .= "MIME-Version: 1.0\n";
    $header .= "From: ${from}\n";
    if (!empty($cc)) {
      $header .= "Cc: {$cc}\n";
    }
    if (!empty($bcc)) {
      $header .= "Bcc: {$bcc}\n";
    }

    if (empty($user_agent)) {
      $user_agent = "PHP/" . phpversion();
    } else {
      $user_agent .= " PHP/" . phpversion();
    }
    $header .= "User-Agent: {$user_agent}\n";
    $header .= $this->getContentType() . "\n";

    return mail($to,
                $subject,
                $body,
                $header,
                '-f' . $sender);
  }

}

/*
 * -*- settings for emacs. -*-
 * Local Variables:
 *   mode:php
 *   indent-tabs-mode: nil
 *   c-basic-offset: 2
 * End:
 */
?>
