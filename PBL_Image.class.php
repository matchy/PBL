<?php
/*
 * Copyright (c) 2004-2005,2013 MACHIDA 'matchy' Hideki
 * Copyright (c) 2006 Japan Computer Co.,Ltd.
 * Copyright (c) 2008 az'Ciel HAKKO Co.,Ltd.
 * All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * 画像ファイルの読み込み、表示、情報取得を行うクラス
 *
 * @package pbl
 */

require_once('PBL_ktai.inc.php');

/**
 * 画像処理クラス
 *
 * @package pbl
 * @access public
 */
class PBL_Image {

  /**
   * ユーザーエージェント情報
   *
   * @access private
   * @var object
   */
  var $agent_;

  /**
   * 元画像フォーマット (IMAGETYPE 定数)
   *
   * @access private
   * @var integer
   */
  var $org_type_;

  /**
   * 元画像サイズ (幅ピクセル)
   *
   * @access private
   * @var integer
   */
  var $org_width_;

  /**
   * 元画像サイズ (高さピクセル)
   *
   * @access private
   * @var integer
   */
  var $org_height_;

  /**
   * クライアント端末に適した表示用画像フォーマット (IMAGETYPE定数)
   *
   * @access private
   * @var integer
   */
  var $disp_type_;

  /**
   * 画像表示用 URL の末尾に不可する文字列 (J-Phone 対応のため)
   *
   * @access private
   * @var string
   */
  var $src_postfix_;

  /**
   * 読み込んだ元画像の画像 ID
   *
   * @access private
   * @var integer
   */
  var $org_image_;

  /**
   * 表示する画像形式の取得.
   *
   * クライアントの端末 (携帯端末) に適合する画像形式を取得する
   *
   * @access private
   * @return integer 表示画像フォーマット (IMAGETYPE定数)
   */
  function getDisplayType() {

    if (isset($this->org_type_)) {
      $result = $this->org_type_;
    } else {
      $result = IMAGETYPE_JPEG;
    }

    if ($this->agent_->isDoCoMo()) {
      if ($this->org_type_ == IMAGETYPE_PNG) {
        if (false/*$this->agent_->getHTMLVersion() >= 4.0*/) {
          // jpeg はキタナくなることがある...
          // PHP の版が新しい場合は gif で出力した方が良さそう
          $result = IMAGETYPE_JPEG;
        } elseif (function_exists('ImageGIF')) {
          $result = IMAGETYPE_GIF;
        } else {
          // GIF を作れない場合は JPEG としておく
          $result = IMAGETYPE_JPEG;
        }
      } elseif ($this->org_type_ == IMAGETYPE_JPEG &&
                function_exists('ImageGIF')) {

        // i-mode HTML 3.0 機種でも jpeg を表示できるものもあるが
        // 切り分けが面倒なので 4.0 以降のみ JPEG 対応として扱う
        if ($this->agent_->getHTMLVersion() < 4.0) {
          $result = IMAGETYPE_GIF;
        }
      }
    }

    elseif ($this->agent_->isVodafone()) {
      if ($this->org_type_ != IMAGETYPE_PNG) {
        if (strcasecmp($this->agent_->getName(), 'j-phone') == 0 &&
            $this->agent_->getVersion() <= 2.0) {

          $result = IMAGETYPE_PNG;
        }
      }
      if ($this->org_type_ == IMAGETYPE_GIF) {
        $result = IMAGETYPE_PNG;
      }
    }

    elseif ($this->agent_->isEZweb()) {
      if ($this->org_type_ == IMAGETYPE_JPEG) {
        // HDML 機でも jpeg 対応のものがあるらしいが、
        // インラインで表示はできないと思われる
        if (!$this->agent_->isXHTMLCompliant()) {
          $this->disp_typ_e = IMAGETYPE_PNG;
        }
      } elseif ($this->org_type_ == IMAGETYPE_GIF) {
        $result = IMAGETYPE_PNG;
      }
    }
    return $result;
  }

  /**
   * 画像 URL の末尾に付加すべき文字列を取得
   *
   * J-Phone の一部機種では、画像 URI の末尾が ".png" でないと
   * 正しく表示できない問題に対処するため
   *
   * @access private
   * @return string 付加文字列
   */
  function getSrcPostfix() {
    $result = '';

    if ($this->agent_->isVodafone()) {
      if ($this->disp_type_ == IMAGETYPE_PNG) {
        $result = "?i=img.png";
      }
    }
    return $result;
  }

  /**
   * 画像の読み込み
   *
   * 指定された画像ファイルを読み込む
   *
   * @access public
   * @param string $image_path 画像ファイルのパス
   */
  function loadImage($image_path) {
    $this->image_path_ = $image_path;
    $data = @GetImageSize($this->image_path_);
    $this->org_type_   = $data[2];
    $this->org_width_  = $data[0];
    $this->org_height_ = $data[1];

    $this->disp_type_   = $this->getDisplayType();
    $this->src_postfix_ = $this->getSrcPostfix();

    switch ($this->org_type_) {
    case IMAGETYPE_GIF:
      $this->org_image_ = @ImageCreateFromGIF($this->image_path_);
      break;
    case IMAGETYPE_JPEG:
      $this->org_image_ = @ImageCreateFromJPEG($this->image_path_);
      break;
    case IMAGETYPE_PNG:
      $this->org_image_ = @ImageCreateFromPNG($this->image_path_);
      break;
    }
    if (!$this->org_image_) {
      // 読み込み失敗時
      $this->org_image_ = ImageCreate(150, 30);
      $bgc = ImageColorAllocate($this->org_image_, 255, 255, 255);
      $tc  = ImageColorAllocate($this->org_image_,0, 0, 0);
      ImageFilledRectangle ($this->org_image_, 0, 0, 150, 30, $bgc);
      $fname = basename($this->image_path_);
      ImageString($this->org_image_, 1, 5, 5,
                  "Error loading {$fname}", $tc);
      $this->org_width_  = 150;
      $this->org_height_ = 30;
    }
  }

  /**
   * 画像処理クラスを構築する
   *
   * $image_path が null の時は読み込みを行わないので、load_image()
   * メソッドを使用して画像を読み込む必要がある
   *
   * @access public
   * @param string $image_path 画像ファイルのパス
   */
  function PBL_Image($image_path=false) {
    $this->agent_ =& Net_UserAgent_Mobile::singleton();
    $this->display_ =& PBL_getDisplayWrapper();

    if (!empty($image_path)) {
      $this->loadImage($image_path);
    }
  }

  /**
   * 表示画像サイズの取得
   *
   * 端末画面サイズよりはみだす画像はそのサイズに収まるように
   * 画像サイズを計算する
   *
   * @access public
   * @param integer $width  実際に表示したいサイズ (省略可)
   * @param integer $height 実際に表示したいサイズ (省略可)
   * @return array array(横ピクセル, 縦ピクセル)
   */
  function calcDisplaySize($width=0, $height=0) {

    if (!$width) {
      $width = $this->org_width_;
    } else {
      $width  = min($this->org_width_,  $width);
    }
    if (!$height) {
      $height = $this->org_height_;
    } else {
      $height = min($this->org_height_, $height);
    }

    if (!$this->agent_->isNonMobile()) {
      $disp_width = $disp_height = 0;
      if (isset($this->display_)) {
        $disp_width  = $this->display_->getWidth();
        $disp_height = $this->display_->getHeight();
      }
      if ($disp_width == 0) {
        $disp_width = $this->org_width_;
      }
      if ($disp_height == 0) {
        $disp_height = $this->org_height_;
      }
      $width  = min($width,  $disp_width);
      $height = min($height, $disp_height);
    }

    if ($width < $height) {
      $new_w = $width;
      $new_h = round(($new_w / $this->org_width_) * $this->org_height_, 0);
      if ($new_h > $height) {
        $new_h = $height;
        $new_w = round(($new_h / $this->org_height_) * $this->org_width_, 0);
      }
    } else {
      $new_h = $height;
      $new_w = round(($new_h / $this->org_height_) * $this->org_width_, 0);
      if ($new_w > $width) {
        $new_w = $width;
        $new_h = round(($new_w / $this->org_width_) * $this->org_height_, 0);
      }
    }
    return array($new_w, $new_h);
  }

  /**
   * 画像サイズを示す HTML ソースの取得
   *
   * 読み込んだ画像のサイズに適合した HTML img タグ用のサイズ指定
   * 属性文字列 (width="www" height="hhh") を取得する
   * 
   * @access public
   * @param integer $width  実際に表示したいサイズ (省略可)
   * @param integer $height 実際に表示したいサイズ (省略可)
   * @return string HTML ソース文字列
   */
  function getHtmlSizeSrc($width=0, $height=0) {
    list($w, $h) = $this->calcDisplaySize($width, $height);
    if ($this->agent_->isNonMobile()) {
      $result  = "width=\"{$w}\" ";
      $result .= "height=\"{$h}\"";
    } else {
      $result =  '';
    }
    return $result;
  }

  /**
   * 画像表示用の img タグの取得
   *
   * 読み込んだ画像のサイズに適合した HTML img タグを取得する。
   * J-Phone の一部機種で画像 URI の末尾が ".png" でないと
   * 正しく表示できない問題を考慮した HTML ソースを出力する
   *
   * <code>
   * 使用例：get_html_src($script='image.php?img=foo.jpg');
   * </code>
   * 
   * @access public
   * @param string $script  表示用 PHP スクリプト名
   * @param string $alt     alt 属性用文字列。null または空文字の場合は
   *                        alt 属性を出力しない (省略可)
   * @param string $border  border 属性用文字列。null または空文字の場合は
   *                        border 属性を出力しない (省略可)
   * @param integer $width  実際に表示したいサイズ (省略可)
   * @param integer $height 実際に表示したいサイズ (省略可)
   * @return string HTML ソース文字列
   */
  function getHtmlSrc($script='image.php', $alt='', $border='',
		      $width=0, $height=0) {
    $postfix = $this->src_postfix_;
    if (!empty($postfix)) {
      if (strchr($script, '?')) {
        $sep = ini_get('arg_separator.output');
        if (!isset($sep)) {
          $sep = '&';
        }
        $postfix = str_replace("?", $sep, $this->src_postfix_);
      }
    }

    $result  = '<img src="' . $script . $postfix . '" ';
    $result .= $this->getHtmlSizeSrc($width, $height);
    if (!empty($alt)) {
      $result .= ' alt="' . $alt . '"';
    }
    if (!empty($border) || $border === '0') {
      $result .= ' border="' . $border . '"';
    }
    $result .= ' />';
    return $result;
  }

  /**
   * 画像の表示
   *
   * クライアントの端末 (携帯端末) に適合する画像形式で、読み込んだ画像を
   * 表示する
   *
   * @access private
   */
  function doDisplay($image, $type) {
    header("Content-type: ". image_type_to_mime_type($type));

    switch ($type) {
    case IMAGETYPE_GIF:
      ImageGIF($image);
      break;
    case IMAGETYPE_JPEG:
      ImageJPEG($image);
      break;
    case IMAGETYPE_PNG:
      ImagePNG($image);
      break;
    }
  }

  /**
   * 画像の表示
   *
   * クライアントの端末 (携帯端末) に適合するサイズ、画像形式で
   * 読み込んだ画像を表示する
   *
   * @access public
   * @param integer $width  実際に表示したいサイズ (省略可)
   * @param integer $height 実際に表示したいサイズ (省略可)
   */
  function display($width=0, $height=0) {
    
    list($w, $h) = $this->calcDisplaySize($width, $height);

    if ($w != $this->org_width_ || $h != $this->org_height_) {
      if (function_exists('ImageCreateTrueColor')) {
        $new_img = ImageCreateTrueColor($w, $h);
      } else {
        $new_img = ImageCreate($w, $h);
      }
      $white = ImageColorAllocate($new_img, 255, 255, 255);
      ImageFilledRectangle($new_img, 0, 0, $w, $h, $white);
      if (function_exists('ImageCopyResampled')) {
        ImageCopyResampled($new_img, $this->org_image_, 0, 0, 0, 0,
                           $w, $h, $this->org_width_,
                           $this->org_height_);
      } else {
        ImageCopyResized($new_img, $this->org_image_, 0, 0, 0, 0,
                         $w, $h, $this->org_width_,
                         $this->org_height_);
      }
#      ImageColorTransparent($new_img, $white);
      $this->doDisplay($new_img, $this->disp_type_);
      ImageDestroy($new_img);

    } else {
      $this->doDisplay($this->org_image_, $this->disp_type_);
    }
  }

  /**
   * 使用リソースの開放
   *
   * 読み込んだ画像を破棄する。本クラスでは PHP の GD 拡張を用いて
   * 画像処理をしている。このメソッドは使用していた GD のリソース
   * を開放するものである
   *
   * @access public
   */
  function free() {
    if ($this->org_image_) {
      ImageDestroy($this->org_image_);
      unset($this->org_image_);
    }
  }

  /**
   * 元画像のフォーマットを取得する
   *
   * @access public
   * @return integer IMAGETYPE定数
   */
  function getOrgType() {
    return $this->org_type_;
  }

}

/*
 * -*- settings for emacs. -*-
 * Local Variables:
 *   mode:php
 *   indent-tabs-mode: nil
 *   c-basic-offset: 2
 * End:
 */
?>
