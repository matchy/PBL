<?php
/*
 * Copyright (c) 2004-2005,2013 MACHIDA 'matchy' Hideki
 * Copyright (c) 2006 Japan Computer Co.,Ltd.
 * All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * 文字列サニタイズ smarty カスタム修飾子
 *
 * @package pbl
 */

require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR .
             'PBL_Utils.inc.php');
require_once('Net/UserAgent/Mobile.php');

/**
 * 文字列サニタイズ smarty カスタム修飾子
 *
 * 例１：文字列をサニタイズする (smarty 標準の escape:"html" では、「'」を
 * 実体参照に置換してしまうため、携帯端末等で問題になる可能性がある)
 * <code>
 * {"<a href=\"http://www.jc-c.co.jp/\">"|PBL_html}
 * </code>
 *
 * 例２：URL やメールアドレスはリンクに変換し、それ以外はサニタイズする
 * <code>
 * {"http://www.jc-c.co.jp/"|PBL_html:"link"}
 * </code>
 *
 * 例３：文字列を強制的に半角に変換し、サニタイズする
 * <code>
 * {"＜a href=\"http://www.jc-c.co.jp/\"＞"|PBL_html:"hankaku"}
 * </code>
 *
 * 例４：「tel:nnn-nnn-nnn」のような文字列があったら、電話番号アンカーにする
 * <code>
 * {"tel:026-225-6000"|PBL_html:"tel"}
 * </code>
 *
 * @access public
 * @param string $string   入力文字列
 * @param string $esc_type パラメータ文字列
 * @return string HTML 出力
 */
function smarty_modifier_PBL_html($string, $esc_type='html') {

  $agent = &Net_UserAgent_Mobile::singleton();
  if (!$agent->isNonMobile()) {
    $buff = mb_convert_kana($string, 'aks');
  } else {
    $buff = $string;
  }

  $buff = $string;

  switch ($esc_type) {
  case 'html':
    return htmlspecialchars($buff);
  case 'link':
    return PBL_escapeAllowLink($buff);
  case 'hankaku':
    return htmlspecialchars(mb_convert_kana($buff, 'aks'));
  case 'tel':
    return PBL_telephoneLink($buff);
  default:
    return $buff;
  }
}

/*
 * -*- settings for emacs. -*-
 * Local Variables:
 *   mode:php
 *   indent-tabs-mode: nil
 *   c-basic-offset: 2
 * End:
 */
?>
