<?php
/*
 * Copyright (c) 2004-2005 MACHIDA Hideki
 * Copyright (c) 2006 Japan Computer Co.,Ltd.
 * Copyright (c) 2008 az'Ciel HAKKO Co.,Ltd.
 * All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * PEAR::HTML_QuickForm ラベル出力用 smarty カスタムタグ (携帯対応)
 *
 * @package pbl
 */

require_once('Net/UserAgent/Mobile.php');
require_once('function.PBL_QFlabel.php');

/**
 * PEAR::HTML_QuickForm ラベル出力用 smarty カスタムタグ (携帯対応)
 *
 * PEAR::HTML_QuickForm のラベル文字列等を出力する
 * <pre>
 * 使用例：{PBL_QFlabelMobile value=$form.foo}
 * </pre>
 * value には各ウィジットに対応する値 ($form 直下の要素) を指定する
 *
 * @access public
 * @param array  $params パラメータ配列
 * @param object &$smarty Smarty オブジェクト
 * @return string HTML 出力
 */
function smarty_function_PBL_QFlabelMobile($params, &$smarty) {

  $agent = &Net_UserAgent_Mobile::singleton();
  if ($agent->isNonMobile()) {
    return smarty_function_PBL_QFlabel($params, $smarty);
  } else {
    $val = $params['value'];
    $result = htmlspecialchars($val['label']);

    if ($val['required']) {
      $result .= '(*)';
    }
    return $result;
  }

}

/*
 * -*- settings for emacs. -*-
 *  Local Variables:
 *  mode:php
 *  indent-tabs-mode: nil
 *  c-basic-offset: 2
 */
?>
