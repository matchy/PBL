PBL
===

概要
----

PHPのバッドノウハウを集めた俺様ライブラリ


依存ライブラリ
--------------

* Smarty-2.6.27
* ADOdb-4992
* PEAR-1.9.4
* PEAR::Auth-1.6.4
* PEAR::QuickForm-3.2.13
* PEAR::HTML_Common-1.2.5
* PEAR::HTML_QuickForm-3.2.13
* PEAR::Net_UserAgent_Mobile-1.0.0
