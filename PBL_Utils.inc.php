<?php
/*
 * Copyright (c) 2004-2005,2013 MACHIDA 'matchy' Hideki
 * Copyright (c) 2006-2007 Japan Computer Co.,Ltd.
 * Copyright (c) 2008 az'Ciel HAKKO Co.,Ltd.
 * Copyright (c) 2011,2012 HAKKO Development Co.,Ltd. az'Ciel Division
 * All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * ユーティリティ関数集.
 *
 * URL文字列処理、サニタイジング、セッション関係の処理を行う。
 *
 * @package pbl
 */

/**
 * サーバールートディレクトリを表す絶対 URL を取得する
 *
 * @access public
 * @param string $use_ssl 取得する URL のスキーマ。
 *                        または 'https://'。null ならば自動判断
 * @return string ルートディレクトリを表す絶対 URL
 */
function PBL_getAbsUrlRootDir($scheme=null) {
  if (empty($scheme)) {
    if ($_SERVER["HTTPS"] == "on") {
      $scheme = "https://";
    } else {
      $scheme = "http://";
    }
  }

  $server_host = $_SERVER["HTTP_HOST"];
  if (empty($server_host)) {
    $server_host = $_SERVER["SERVER_NAME"];
  }
  return $scheme . $server_host . "/" ;
}

/**
 * 実行スクリプトと同じ階層のディレクトリを表す絶対 URL を取得する
 *
 * pathinfo が指定されている場合等、本関数を使用することで本来の
 * ディレクトリを取得することが可能
 *
 * @access public
 * @param string $scheme 取得する URL のスキーマ。
 *                       'http://' または 'https://'。null ならば自動判断
 * @return string カレントディレクトリを表す絶対 URL
 */
function PBL_getAbsUrlCurrentDir($scheme=null) {
  $root = PBL_getAbsUrlRootDir($scheme);

  $root = substr($root, 0, strlen($root) - 1);

  if (preg_match('@(^.*/).*@', $_SERVER["SCRIPT_NAME"], $matches)) {
    $dir = $matches[1];
  } else {
    $dir = "/";
  }

  return $root . $dir;
}

/**
 * 実行スクリプトを表す絶対 URL を取得する
 *
 * @access public
 * @param string $scheme 取得する URL のスキーマ。
 *                       'http://' または 'https://'。null ならば自動判断
 * @return string カレントファイルを表す絶対 URL
 */
function PBL_getAbsUrlCurrentFile($scheme=null) {
  $root = PBL_getAbsUrlRootDir($scheme);

  $root = substr($root, 0, strlen($root) - 1);

  return $root . $_SERVER["SCRIPT_NAME"];
}

/**
 * 指定された URL に SID を付加する
 *
 * @access public
 * @param string  $url URL 文字列
 * @return string SIDが付加された URL 文字列
 */
function PBL_addSidToUrl($url) {
  $result = $url;

  $invalid_pattern = "@^(mailto:|tel:|http://|https://|ftp://|file:|#)@";
  if (!preg_match($invalid_pattern, $url)) {

    if (defined('SID')) {
      $sid = SID;
      if (!empty($sid)) {
        if (preg_match("/\?/", $url)) {
          $sep = ini_get('arg_separator.output');
          if (!isset($sep)) {
            $sep = '&';
          }
          $result .= "{$sep}{$sid}";
        } else {
          $result .= "?{$sid}";
        }
      }
    }
  }

  return $result;
}

/**
 * 相対 URL から絶対 URL を取得する
 *
 * 基本的には同一ディレクトリのファイルをファイル名だけ引数に書いて使う。
 * 「..」の使用は推奨しない。
 *
 * @access public
 * @param string $url     相対 URL 文字列
 * @param string $scheme 取得する URL のスキーマ。
 *                       'http://' または 'https://'。null ならば自動判断
 * @param string $add_sid true:SIDを付加する / false:しない
 * @return string 絶対 URL 文字列
 */
function PBL_getAbsUrl($url, $scheme=null, $add_sid=true) {
  $invalid_pattern = "@^(mailto:|tel:|http://|https://|ftp://|file:)@";
  if (preg_match($invalid_pattern, $url)) {
    $result = $url;

  } elseif (preg_match("@^/(.*)$@", $url, $matches)) {
    $result = PBL_getAbsUrlRootDir($scheme) . $matches[1];

  } elseif (preg_match("@^#.*$@", $url)) {
    $result = PBL_getAbsUrlCurrentFile($scheme) . $url;

  } else {
    $result = PBL_getAbsUrlCurrentDir($scheme) . $url;

  }

  $result = preg_replace('@/[^/]+/\\.\\./@', '/', $result);

  if ($add_sid) {
    $result = PBL_addSidToUrl($result);
  }

  return $result;
}

/**
 * Location ヘッダで forward する
 *
 * 指定されたファイル (ex. foo.php) を絶対 URL に変換し、さらに
 * セッション使用時は SID を付加して、Location レスポンスヘッダ
 * でクライアントのアクセス先を転送する
 * <code>
 * 使用例：PBL_forwardLocation('foo.php');
 * </code>
 *
 * @access public
 * @param string $url URL 文字列
 * @param string $scheme 取得する URL のスキーマ。
 *                       'http://' または 'https://'。null ならば自動判断
 */
function PBL_forwardLocation($url, $scheme=null) {

  $sid = SID;
  if (!empty($sid) && strpos($url, $sid)) {
    $forward_url = PBL_getAbsUrl($url, $scheme, false);
  } else {
    $forward_url = PBL_getAbsUrl($url, $scheme, true);
  }

  if (strstr($_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS')) {
    header("Refresh: 0;url={$forward_url}");
  } else {
    header("Location: {$forward_url}");
  }
}

/**
 * SID を除いた Request URI を取得する
 *
 * @access public
 * @return string 変換された Request URI
 */
function PBL_getRequestUrl() {

  $result = $_SERVER['REQUEST_URI'];
  $sid = session_name() . "=" . session_id();
  if (!empty($sid)) {
    while (($pos = strpos($result, $sid)) !== false) {
      $result = substr($result, 0, $pos-1);
    }
  }
  return $result;
}

/**
 * 文中の URL やメールアドレスはリンクにし、それ以外をサイニタイズする
 *
 * @access public
 * @param string $text 入力文字列
 * @param boolean $blanktarget true の場合、http(s) で始まる URL を
 *                             target="_blank" でリンクする
 * @return string 変換済文字列
 */
function PBL_escapeAllowLink($text, $blanktarget=true) {

  $url_pattern = '/(((?:https?|ftp):[()%#!\\/0-9a-z_$@.&+-,\'"*=;?:~-]+)|([0-9a-z_.-]+@[()%!0-9a-z_$.&+-,\'"*-]+\\.[()%!0-9a-z_$.&+-,\'"*-]+))/i';

  $intencode = mb_internal_encoding();
  if (preg_match('/^sjis/i', $intencode)) {
    $do_utf = true;
    $buf = mb_convert_encoding($text, 'UTF-8');
  } else {
    $do_utf = false;
    $buf = $text;
  }

  $buf = preg_replace("/ /", "\001", $buf);
  $buf = preg_replace("/</", "\002", $buf);
  $buf = preg_replace("/>/", "\003", $buf);
  $buf = preg_replace("/\"/", "\004", $buf);
  $buf = preg_replace("/&/", '&amp;', $buf);
  if (preg_match_all($url_pattern, $buf, $matches, PREG_SET_ORDER)) {
    foreach ($matches as $match) {
      if ($match[0] == $match[1]) {
        $url = preg_replace('/:/', "\005", $match[1]);
        if (preg_match("<^https?://>", $match[0]) && $blanktarget) {
          $buf = preg_replace('/'.preg_quote($match[0],'/').'/',
                              "<a href=\"{$url}\" target=\"_blank\">{$url}</a>",
                              $buf);
        } else {
          $buf = preg_replace('/'.preg_quote($match[0],'/').'/',
                              "<a href=\"{$url}\">{$url}</a>",
                              $buf);
        }
      } else {
        $addr = preg_replace('/@/', "\006", $match[2]);
        $buf = preg_replace('/'.preg_quote($match[0],'/').'/',
                            "<a href=\"mailto:{$addr}\">{$addr}</a>",
                            $buf);
      }
    }
  }

  $buf = preg_replace("/\006/", '@', $buf);
  $buf = preg_replace("/\005/", ':', $buf);
  $buf = preg_replace("/\004/", '&quot;', $buf);
  $buf = preg_replace("/\003/", '&gt;', $buf);
  $buf = preg_replace("/\002/", '&lt;', $buf);
  $buf = preg_replace("/\001/", ' ', $buf);

  if ($do_utf) {
    $buf = mb_convert_encoding($buf, $intencode, 'UTF-8');
  }

  return $buf;
}

/**
 * 「tel:nnn-nnn-nnn」のような文字列があったら、電話番号アンカーにする
 *
 * @access public
 * @param string $text 入力文字列
 * @return string 変換済文字列
 */
function PBL_telephoneLink($text) {
  $pattern = '/tel:([0-9\-*#]+)/';
  $buf = preg_replace($pattern,
                      "<a href=\"tel:$1\">$1</a>",
                      $text);
  return $buf;
}

/**
 * session id の切り替え (sessoin ハイジャック対策)
 *
 * @access public
 */
function PBL_switchSession() {
  global $_SESSION;
  $buf = serialize($_SESSION);
  $_SESSION = array();
  // session_destroy();

  if (function_exists('adodb_session_regenerate_id')) {
    adodb_session_regenerate_id();
  } else {
    if (version_compare(PHP_VERSION, '5.1.0') >= 0) {
      session_regenerate_id(TRUE);
    } else {
      $old_id = session_id();
      session_regenerate_id();
      unlink(session_save_path() . 'sess_' . $old_id);
    }
  }
  $_SESSION = unserialize($buf);
}

/**
 * ファイルの内容を eval してその結果を取得する。
 *
 * @access public
 * @param string $file ソ－スファイルへのパス
 * @return string 実行結果
 */
function PBL_evalFile($file) {
  $result = "";
  if ($fp = fopen($file, "r")) {
    $buf = fread($fp, filesize($file));
    fclose($fp);

    $buf = mb_convert_encoding($buf, mb_internal_encoding(), "auto");
    ob_start();
    eval("?>" . $buf . "\n");
    $result = ob_get_contents();
    ob_end_clean();
  }
  return $result;
}

/**
 * でたらめな文字列の生成
 *
 * @access public
 * @param integer $len 生成する文字列の文字数
 * @return string 生成された文字列
 */
function PBL_makeRandomString($len) {
  static $seed = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

  list($usec, $sec) = explode(' ', microtime());
  srand((float) $sec + ((float) $usec * 100000));

  $result = "";
  for ($i=0 ; $i<$len ; $i++) {
    $result .= $seed[rand(0, strlen($seed)-1)];
  }
  return $result;
}

/**
 * ブラウザにキャッシュさせないためのレスポンスヘッダを出力する
 *
 * @access public
 */
function PBL_putNocacheHeader() {
  header('Expires: '. date(DATE_RFC822));
  header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
  header('Pragma: no-cache');
}

/**
 * XML スキーマの dateTime 型文字列をタイムスタンプに変換する
 *
 * @param string $input 入力文字列
 * @return integer UNIX エポックタイムからの経過ミリ秒
 */
function PBL_parseXMLtimestamp($input) {
  // PHP-4 に対応するため、strptime() を使わずに自力で対応
  $regexp = '/^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})T([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})/';

  if (preg_match($regexp, $input, $matches) > 0) {
    $result = @mktime($matches[4],  // 時
                      $matches[5],  // 分
                      $matches[6],  // 秒
                      $matches[2],  // 月
                      $matches[3],  // 日
                      $matches[1]); // 年
    if ($result > 0) {
      return $result;
    }
  }

  // 解析できなかった場合は現在時刻を返す
  return time();
}

/**
 * HTTP エラーレスポンスを返す
 *
 * @param string $msg メッセージ
 * @param integer $result リザルトコード
 */
function PBL_httpError($msg, $result) {
  header($msg, true, $result);
  header('Content-Type: text/plain');
  echo $msg;
  exit;
}

/**
 * HTTP 配列の全要素を HTML エスケープする
 *
 * @param array input 入力配列
 * @return エスケープ済み配列
 */
function PBL_escapeArray($input) {
  $result = array();
  foreach ($input as $key => $value) {
    if (is_array($value)) {
      $result[$key] = PBL_escape_array($value);
    } else {
      $result[$key] = htmlspecialchars($value);
    }
  }
  return $result;
}

/*
 * -*- settings for emacs. -*-
 * Local Variables:
 *   mode:php
 *   indent-tabs-mode: nil
 *   c-basic-offset: 2
 * End:
 */
?>
