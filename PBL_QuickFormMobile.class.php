<?php
/*
 * Copyright (c) 2004-2005,2015 MACHIDA 'matchy' Hideki
 * Copyright (c) 2006 Japan Computer Co.,Ltd.
 * Copyright (c) 2008 az'Ciel HAKKO Co,Ltd.
 * All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * HTML_QuickForm ラッピングクラス.
 *
 * PBL_QuickForm を携帯対応にしたもの
 *
 * @package pbl
 */

require_once('HTML/QuickForm.php');
require_once('HTML/QuickForm/textarea.php');
require_once('HTML/QuickForm/checkbox.php');
require_once('HTML/QuickForm/radio.php');
require_once('PBL_QuickForm.class.php');
require_once('PBL_ktai.inc.php');

/**
 * QuickForm クラス.
 *
 * PEAR::HTML_QuickForm のメッセージ部分を日本語にしたもの。
 * 要 PEAR::Net_UserAgent_Mobile
 *
 * @package pbl
 * @access public
 */
class PBL_QuickFormMobile extends PBL_QuickForm {

  /**
   * メッセージを日本語に設定した HTML_QuickForm 派生クラスを
   * 構築する。
   *
   * @access public
   * @see HTML_QuickForm::HTML_QuickForm()
   */
  function PBL_QuickFormMobile($formName='', $method='',
			       $action='',
			       $target='_self', $attributes=null,
			       $trackSubmit = false) {

    if (empty($method)) {
      $method = PBL_getFormMethod();
    }

    parent::PBL_QuickForm($formName, $method,
                           $action, $target, $attributes,
                           $trackSubmit);

    $agent =& Net_UserAgent_Mobile::singleton();
    if (!$agent->isNonMobile()) {
      $requirednote  = '<FONT color="#FF0000">*</FONT> ';
      $requirednote .= 'が付いている項目は必ずご入力ください。';
    }
    $this->setRequiredNote($requirednote);
  }

}

/**
 * HTML_QuickForm_checkbox の携帯対応修正版
 *
 * 携帯端末の場合には <label> タグを出力しないようにもしている
 *
 * 標準の HTML_QuickForm_checkbox と置き換わって動作するため、特に
 * 意識すること無く本クラスが使用される。
 *
 * @package pbl
 * @access public
 */
class PBL_QuickFormMobile_checkbox extends PBL_QuickForm_checkbox {

  /**
   * QuickForm チェックボックスの構築
   *
   * @access public
   * @see PBL_QuickForm_checkbox::PBL_QuickForm_checkbox()
   */
  function PBL_QuickFormMobile_checkbox($elementName=null,
					$elementLabel=null,
					$text='',
					$attributes=null) {

    parent::PBL_QuickForm_checkbox($elementName,
				   $elementLabel, $text, $attributes);
  }

  /**
   * HTML レンダリングメソッド
   *
   * @access public
   * @see HTML_QuickForm_checkbox::toHtml()
   */
  function toHtml() {
    $agent =& Net_UserAgent_Mobile::singleton();
		
    if ($agent->isNonMobile()) {
      return parent::toHtml();
    } else {
      return HTML_QuickForm_input::toHtml() . $this->_text;
    }
  }

}

/**
 * HTML_QuickForm_radio の携帯対応修正版
 *
 * 携帯端末の場合には <label> タグを出力しないようにしている
 *
 * 標準の HTML_QuickForm_radio と置き換わって動作するため、特に
 * 意識すること無く本クラスが使用される。
 *
 * @package pbl
 * @access public
 */
class PBL_QuickFormMobile_radio extends HTML_QuickForm_radio {

  /**
   * QuickForm ラジオボックスの構築
   *
   * @access public
   * @see HTML_QuickForm_radio::HTML_QuickForm_radio()
   */
  function PBL_QuickFormMobile_radio($elementName=null,
				     $elementLabel=null,
				     $text='',
				     $value=null,
				     $attributes=null) {

    parent::HTML_QuickForm_radio($elementName, $elementLabel,
                                 $text, $value, $attributes);

  }

  /**
   * HTML レンダリングメソッド
   *
   * @access public
   * @see HTML_QuickForm_radio::toHtml()
   */
  function toHtml() {
    $agent =& Net_UserAgent_Mobile::singleton();
		
    if ($agent->isNonMobile()) {
      return parent::toHtml();
    } else {
      return HTML_QuickForm_input::toHtml() . $this->_text;
    }
  }
}

// 派生クラスで機能を置き換える
$GLOBALS['HTML_QUICKFORM_ELEMENT_TYPES']['checkbox'] =
  array(__FILE__, 'PBL_QuickFormMobile_checkbox');
$GLOBALS['HTML_QUICKFORM_ELEMENT_TYPES']['radio'] =
  array(__FILE__, 'PBL_QuickFormMobile_radio');

/*
 * -*- settings for emacs. -*-
 * Local Variables:
 *   mode:php
 *   indent-tabs-mode: nil
 *   c-basic-offset: 2
 * End:
 */
?>
