<?php
/*
 * Copyright (c) 2004-2005,2013 MACHIDA 'matchy' Hideki
 * Copyright (c) 2006 Japan Computer Co.,Ltd.
 * Copyright (c) 2008 az'Ciel HAKKO Co.,Ltd.
 * All Rights Reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * 携帯端末対応関連関数集.
 *
 * 携帯端末に依存した処理を行う。
 * 要 PEAR::Net_UserAgent_Mobile。
 *
 * @package pbl
 */

require_once('Net/UserAgent/Mobile.php');

/**
 * ディスプレイサイズの取得
 * DoCoMo i-mode エミュレータに対応
 *
 * @access public
 * @return object Display オブジェクト
 */
function &PBL_getDisplayWrapper() {
  $agent = &Net_UserAgent_Mobile::singleton();

  if ($agent->isNonMobile()) {
    return null;
  }

  $result = $agent->getDisplay();
  if ($agent->isDoCoMo()) {
    if (preg_match("/ISIM/", $agent->getModel())) {
      if ($result->_widthBytes > 0 && $result->_heightBytes > 0) {
        $result->_width  = $result->_widthBytes  * 16;
        $result->_height = $result->_heightBytes * 16;
        if ($agent->getModel() == 'ISIM60') {
          $result->_widthBytes *= 2;
        } else {
          $result->_width /= 2;
        }
      } else {
        $result->_width  = 160;
        $result->_height = 160;
        $result->_widthBytes  = 20;
        $result->_heightBytes = 10;
      }
    }
  }
  return $result;
}

/**
 * 端末に合わせて数字を表わす絵文字を取得する
 *
 * @access public
 * @param integer $number 数値 (0〜9)
 * @return string 絵文字表示用 HTML コード
 */
function PBL_getNumSymbol($number) {
  $agent =& Net_UserAgent_Mobile::singleton();

  $result = "";

  if ($agent->isDoCoMo()) {
    if ($number == 0) {
      $buf = "\xF9\x90";
    } elseif ($number >= 1 && $number <= 9) {
      $buf = "\xF9" . chr(0x87 + $number - 1);
    } else {
      $buf = "";
    }
    $result = mb_convert_encoding($buf, mb_internal_encoding(), "SJIS-win");

  } elseif ($agent->isVodafone()) {
    if (strcasecmp($agent->getName(), 'j-phone') == 0 &&
        $agent->getVersion() <= 2.0) {
      $result = '';
    } else {
      if ($number == 0) {
        $result = "\x1B\x24\x46\x45\x0F";
      } elseif ($number >= 1 && $number <= 9) {
        $result = pack("H6c*", "1B2446", $number + 0x3C - 1, 0x0F);
      }
    }

  } elseif ($agent->isEZweb()) {
    if ($number == 0) {
      $buf = 325;
    } elseif ($number >= 1 && $number <= 9) {
      $buf = $number + 180 - 1;
    }

    if ($agent->isXHTMLCompliant()) {
      $result = "<img localsrc=\"{$buf}\">";
    } else {
      // $result = "<img icon=\"{$buf}\">";
      $result = "[{$number}]";
    }
  } else {
    $result = "[{$number}]";
  }
  return $result;
}

/**
 * 端末に合わせてアクセスキー用のHTML属性文字列を取得する
 *
 * @access public
 * @param string $key アクセスキー
 * @return string アクセスキー用 HTML 属性文字列
 */
function PBL_getAccesskeyAttr($key) {
  $agent =& Net_UserAgent_Mobile::singleton();

  if ($agent->isVodafone()) {
    if (strcasecmp($agent->getName(), 'j-phone') == 0 &&
        $agent->getVersion() <= 2.0) {
      $result = 'directkey="' . $key . '"';
    } else {
      $result = 'directkey="' . $key . '" nonumber="1"';
    }
  } else {
    $result = 'accesskey="' . $key . '"';
  }
  return $result;
}

/**
 * 端末に合わせて入力文字種類用のHTML属性を取得する.
 *
 * 入力文字種類に指定できるのは以下の各値.
 * <pre>
 * 'kana'    -- 全角かな
 * 'hankana' -- 半角かな
 * 'roman'   -- 半角英数字
 * 'numeric' -- 半角数字
 * </pre>
 *
 * @access public
 * @param string $mode 入力文字種類
 * @return array 入力文字種用 HTML 属性名がキー、設定値が値の連想配列
 */
function PBL_getInputmodeAttr($mode) {
  $agent =& Net_UserAgent_Mobile::singleton();

  $result = '';

  if ($agent->isDoCoMo()) {
    switch ($mode) {
    case 'kana':
      $result = array('istyle', 1);
      break;
    case 'hankana':
      $result = array('istyle', 2);
      break;
    case 'roman':
      $result = array('istyle', 3);
      break;
    case 'numeric':
      $result = array('istyle', 4);
      break;
    }
  } elseif ($agent->isVodafone()) {
    switch ($mode) {
    case 'kana':
      $result = array('mode', 'hiragana');
      break;
    case 'hankana':
      $result = array('mode', 'hankakukana');
      break;
    case 'roman':
      $result = array('mode', 'alphabet');
      break;
    case 'numeric':
      $result = array('mode', 'numeric');
      break;
    }
  } elseif ($agent->isEZweb()) {
    switch ($mode) {
    case 'kana':
      $result = array('format', '*M');
      break;
    case 'hankana':// 対応するものがないので全角
      $result = array('format', '*M');
      break;
    case 'roman':
      $result = array('format', '*m');
      break;
    case 'numeric':
      $result = array('format', '*N');
      break;
    }
  }
  return $result;
}

/**
 * form の method 属性値を取得する
 *
 * J-Phone 初期端末やエミュレータでは post メソッドが使えないので、
 * この関数経由でメソッド値を取得する。
 * 基本的には post が返却される
 *
 * @access public
 * @return string "post" または "get"
 */
function PBL_getFormMethod() {
  $agent =& Net_UserAgent_Mobile::singleton();

  if ($agent->isVodafone() &&
      strcasecmp($agent->getName(), 'j-phone') == 0 &&
      $agent->getVersion() <= 2.0) {

    $result = "get";
  } else {
    $result = "post";
  }
  return $result;
}


/**
 * 携帯端末用にセッション関係の初期化を行う
 *
 * 携帯端末ではクッキーを使わないように設定する。
 * 最近の携帯はクッキーが使えるものもあるが、SSL 経由では
 * 制限がある場合がある。
 * php.ini 等で session.use_trans_sid が 1 にセットされている
 * ことが前提である。
 *
 * @access private
 */
function PBL_initSesseionForMobile() {
  $agent =& Net_UserAgent_Mobile::singleton();
  if ($agent->isNonMobile()) {
    ini_set('session.use_cookies',      '1');
    ini_set('session.use_only_cookies', '1');
    ini_set('url_rewriter.tags', '');
  } else {
    ini_set('session.use_cookies',      '0');
    ini_set('session.use_only_cookies', '0');
    ini_set('url_rewriter.tags',
            'a=href,area=href,frame=src,input=src,form=fakeentry');
  }
}

/**
 * 携帯からのリクエスト値の文字コードを変換する
 *
 * 携帯からの入力値をsjis-WINからUTF-8に変換する
 *
 * @param array $input 入力値配列
 * @access private
 */
function PBL_convertInputEncoding($input) {
  $agent =& Net_UserAgent_Mobile::singleton();
  if (!$agent->isNonMobile()) {
    foreach ($input as $key => $value) {
      if (is_string($value)) {
	$input[$key] = mb_convert_encoding($value, 'UTF-8', 'auto');
      } else if (is_array($value)) {
	$input[$key] = PBL_convertInputEncoding($value);
      } else {
	$input[$key] = $value;
      }
    }
  }
  return $input;
}

PBL_initSesseionForMobile();
$_REQUEST = PBL_convertInputEncoding($_REQUEST);
$_GET = PBL_convertInputEncoding($_GET);
$_POST = PBL_convertInputEncoding($_POST);

/*
 * -*- settings for emacs. -*-
 * Local Variables:
 *   mode:php
 *   indent-tabs-mode: nil
 *   c-basic-offset: 2
 * End:
 */
?>
